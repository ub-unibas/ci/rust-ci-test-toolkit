#!/bin/bash

rust-cov export \
--format lcov \
--ignore-filename-regex='/usr/local/cargo/registry' \
--ignore-filename-regex='rustc' $( \
      for file in \
        $( \
          RUSTFLAGS="-C instrument-coverage" \
            cargo test --tests --no-run --message-format=json \
              | jq -r "select(.profile.test == true) | .filenames[]" \
              | grep -v dSYM - \
        ); \
      do \
        printf "%s %s " -object $file; \
      done \
    ) \
--instr-profile=app.profdata \
--Xdemangler=rustfilt
