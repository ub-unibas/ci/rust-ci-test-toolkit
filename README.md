# Toolkit for Testing and Building Rust Applications

This repository provides a Docker image which contains tools for testing and building Rust applications especially in a CI/CD context.

- [cargo](https://github.com/rust-lang/cargo) build tool with targets `x86_64-unknown-linux-gnu` and `x86_64-unknown-linux-musl`
- Utils for generating coverage reports in GitLab
- [upx](https://github.com/upx/upx) for binary packaging
- [cargo-audit](https://github.com/rustsec/rustsec/blob/main/cargo-audit/README.md)
  for scanning the code for security vulnerabilities
- [Miri](https://github.com/rust-lang/miri) for detecting undefined behavior in
  the code

## Usage

If used in a GitLab CI/CD context, besides the mentioned binaries the following
util scripts are available:

- `coverage_report.sh`: Summarise instrprof style coverage information
- `coverage_export.sh` Export instrprof file to structured format
- `run.sh`: Builds code and creates coverage report

If the image is run standalone:

```sh
docker run -v.:/workdir/ gl-ci-rust-coverage
```
