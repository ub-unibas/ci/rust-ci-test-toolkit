FROM rust:1.84.1-bookworm
ARG UPX_VERSION=4.2.4
CMD "/usr/bin/run.sh"
ENV BUILDDIR "/builds"
# ENV RUSTFLAGS "-C instrument-coverage"
ENV PATH "$PATH:/root/.local/bin"
WORKDIR $BUILDDIR
RUN apt-get update && \
    apt-get install -y git python3 python3-pip pipx jq musl-tools && \
    apt-get clean && \
    pipx install lcov_cobertura && \
    cargo install --locked rustfilt cargo-audit cargo-binutils cargo-nextest cargo-cyclonedx && \
    rustup target add x86_64-unknown-linux-musl && \
    rustup component add llvm-tools-preview clippy && \
    rustup toolchain install nightly --component miri && \
    wget https://github.com/upx/upx/releases/download/v${UPX_VERSION}/upx-${UPX_VERSION}-amd64_linux.tar.xz && \
    tar xJf upx-${UPX_VERSION}-amd64_linux.tar.xz && \
    mv upx-${UPX_VERSION}-amd64_linux/upx /usr/bin/ && \
    rm -rf upx-${UPX_VERSION}-amd64_linux
COPY ./coverage_report.sh /usr/bin/
COPY ./coverage_export.sh /usr/bin/
COPY ./run.sh /usr/bin/
