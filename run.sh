#!/bin/bash

echo "Workdir:" `pwd`
cargo test --tests
rust-profdata merge -sparse default_*.profraw -o app.profdata
coverage_report.sh
coverage_export.sh > out.lcov
lcov_cobertura out.lcov
rm -f *.profraw app.profdata out.lcov
